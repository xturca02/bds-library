package org.vut.feec.library.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.vut.feec.library.api.MemberDetailView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MemberDetailViewController {
    private static final Logger logger = LoggerFactory.getLogger(MemberDetailViewController.class);

    @FXML
    private TextField tfmember_id;

    @FXML
    private TextField tfmail;

    @FXML
    private TextField tfname;

    @FXML
    private TextField tfsurname;

    @FXML
    private TextField tfcity;

    @FXML
    private TextField tfhouseNumber;

    @FXML
    private TextField tfstreet;

    // used to reference the stage and to get passed data through it
    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        tfmember_id.setEditable(false);
        tfmail.setEditable(false);
        tfname.setEditable(false);
        tfsurname.setEditable(false);
        tfcity.setEditable(false);
        tfhouseNumber.setEditable(false);
        tfstreet.setEditable(false);

        loadMembersData();

        logger.info("PersonsDetailViewController initialized");
    }

    private void loadMembersData() {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof MemberDetailView) {
            MemberDetailView personBasicView = (MemberDetailView) stage.getUserData();
            tfmember_id.setText(String.valueOf(personBasicView.getMember_id()));
            tfmail.setText(personBasicView.getMail());
            tfname.setText(personBasicView.getName());
            tfsurname.setText(personBasicView.getSurname());
            tfcity.setText(personBasicView.getCity());
            tfhouseNumber.setText(personBasicView.gethouseNumber());
            tfstreet.setText(personBasicView.getStreet());
        }
    }

}
