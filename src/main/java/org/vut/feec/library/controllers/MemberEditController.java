package org.vut.feec.library.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.vut.feec.library.api.MemberBasicView;
import org.vut.feec.library.api.MemberEditView;
import org.vut.feec.library.data.MemberRepository;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vut.feec.library.services.MemberService;

import java.util.Optional;

public class MemberEditController {
    private static final Logger logger = LoggerFactory.getLogger(MemberEditController.class);

    @FXML
    public Button editMemberButton;
    @FXML
    public TextField tfmember_id;
    @FXML
    private TextField tfname;
    @FXML
    private TextField tfsurname;
    @FXML
    private TextField tfmail;

    private MemberService memberService;
    private MemberRepository memberRepository;
    private ValidationSupport validation;

    // used to reference the stage and to get passed data through it
    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        memberRepository = new MemberRepository();
        memberService = new MemberService(memberRepository);

        validation = new ValidationSupport();
        validation.registerValidator(tfmember_id, Validator.createEmptyValidator("The id must not be empty."));
        tfmember_id.setEditable(false);
        validation.registerValidator(tfname, Validator.createEmptyValidator("The name must not be empty."));
        validation.registerValidator(tfsurname, Validator.createEmptyValidator("The surname must not be empty."));
        validation.registerValidator(tfmail, Validator.createEmptyValidator("The mail must not be empty."));

        editMemberButton.disableProperty().bind(validation.invalidProperty());

        loadPersonsData();

        logger.info("MemberEditController initialized");
    }

    private void loadPersonsData() {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof MemberBasicView) {
            MemberBasicView memberBasicView = (MemberBasicView) stage.getUserData();
            tfmember_id.setText(String.valueOf(memberBasicView.getMember_Id()));
            tfname.setText(memberBasicView.getMail());
            tfsurname.setText(memberBasicView.getName());
            tfmail.setText(memberBasicView.getSurname());
        }
    }

    @FXML
    public void handleEditMemberButton(ActionEvent event) {
        // can be written easier, its just for better explanation here on so many lines
        Long member_id = Long.valueOf(tfmember_id.getText());
        String name = tfname.getText();
        String surname = tfsurname.getText();
        String mail = tfmail.getText();

        MemberEditView memberEditView = new MemberEditView();
        memberEditView.setId(member_id);
        memberEditView.setName(name);
        memberEditView.setSurname(surname);
        memberEditView.setMail(mail);

        memberService.editMember(memberEditView);

        personEditedConfirmationDialog();
    }

    private void personEditedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Member Edited Confirmation");
        alert.setHeaderText("Your member was successfully edited.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }

}
