package org.vut.feec.library.controllers;



import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.vut.feec.library.api.MemberFilterView;
import org.vut.feec.library.data.MemberRepository;
import org.vut.feec.library.services.MemberService;
import java.util.List;

public class MemberFilterController {
    @FXML
    public TableColumn<MemberFilterView, Long> member_id;
    @FXML
    public TableColumn<MemberFilterView, String> name;
    @FXML
    public TableColumn<MemberFilterView, String> surname;
    @FXML
    public TableColumn<MemberFilterView, String> mail;
    @FXML
    public TableView<MemberFilterView> systemPersonsTableView;

    public Stage stage;
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    private MemberService memberService;
    private MemberRepository memberRepository;
    @FXML
    private void initialize(){
        memberRepository = new MemberRepository();
        memberService = new MemberService(memberRepository);

        member_id.setCellValueFactory(new PropertyValueFactory<MemberFilterView, Long>("member_id"));
        name.setCellValueFactory(new PropertyValueFactory<MemberFilterView, String>("name"));
        surname.setCellValueFactory(new PropertyValueFactory<MemberFilterView, String>("surname"));
        mail.setCellValueFactory(new PropertyValueFactory<MemberFilterView, String>("mail"));

        ObservableList<MemberFilterView> observablePersonList = initializePersonsData();
        systemPersonsTableView.setItems(observablePersonList);

    }

    private ObservableList<MemberFilterView> initializePersonsData() {

        String text = (String) stage.getUserData();
        System.out.println("controller "+text);
        List<MemberFilterView> members = memberService.getMemberFilterView(text);
        return FXCollections.observableArrayList(members);
    }
}