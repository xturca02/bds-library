package org.vut.feec.library.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.util.Duration;
import org.vut.feec.library.api.MemberCreateView;
import org.vut.feec.library.data.MemberRepository;
import org.vut.feec.library.services.MemberService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;


public class MemberCreateController {

    private static final Logger logger = LoggerFactory.getLogger(MemberCreateController.class);

    @FXML
    public Button newPersonCreatePerson;
    @FXML
    private TextField newPersonEmail;

    @FXML
    private TextField newPersonFirstName;

    @FXML
    private TextField newPersonLastName;

    @FXML
    private TextField newPersonNickname;

    @FXML
    private TextField newPersonPwd;

    private MemberService memberService;
    private MemberRepository memberRepository;
    private ValidationSupport validation;

    @FXML
    public void initialize() {
        memberRepository = new MemberRepository();
        memberService = new MemberService(memberRepository);

        validation = new ValidationSupport();
        validation.registerValidator(newPersonEmail, Validator.createEmptyValidator("The email must not be empty."));
        validation.registerValidator(newPersonFirstName, Validator.createEmptyValidator("The name must not be empty."));
        validation.registerValidator(newPersonLastName, Validator.createEmptyValidator("The surname must not be empty."));
        validation.registerValidator(newPersonPwd, Validator.createEmptyValidator("The password must not be empty."));


        newPersonCreatePerson.disableProperty().bind(validation.invalidProperty());

        logger.info("PersonCreateController initialized");
    }

    @FXML
    void handleCreateNewPerson(ActionEvent event) {
        // can be written easier, its just for better explanation here on so many lines
        String mail = newPersonEmail.getText();
        String name = newPersonFirstName.getText();
        String surname = newPersonLastName.getText();
        String password = newPersonPwd.getText();

        MemberCreateView memberCreateView = new MemberCreateView();
        memberCreateView.setPwd(password.toCharArray());
        memberCreateView.setMail(mail);
        memberCreateView.setName(name);
        memberCreateView.setSurname(surname);

        memberService.createMember(memberCreateView);

        personCreatedConfirmationDialog();
    }

    private void personCreatedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Person Created Confirmation");
        alert.setHeaderText("Your person was successfully created.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }



}
