package org.vut.feec.library.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.vut.feec.library.App;
import org.vut.feec.library.api.MemberBasicView;
import org.vut.feec.library.api.MemberDetailView;
import org.vut.feec.library.data.MemberRepository;
import org.vut.feec.library.exceptions.ExceptionHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.vut.feec.library.services.MemberService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;


public class MemberController {

    private static final Logger logger = LoggerFactory.getLogger(MemberController.class);

    @FXML
    public Button addPersonButton;
    @FXML
    public Button refreshButton;
    @FXML
    public Button filterButton;
    @FXML
    public TextField searchBar;
    @FXML
    private TableColumn<MemberBasicView, Long> member_id;
    @FXML
    private TableColumn<MemberBasicView, String> city;
    @FXML
    private TableColumn<MemberBasicView, String> mail;
    @FXML
    private TableColumn<MemberBasicView, String> surname;
    @FXML
    private TableColumn<MemberBasicView, String> name;
    @FXML
    private TableView<MemberBasicView> systemPersonsTableView;
    @FXML
    public MenuItem exitMenuItem;

    private MemberService memberService;
    private MemberRepository memberRepository;

    public MemberController() {
        member_id = new TableColumn<MemberBasicView, Long>();
        city = new TableColumn<MemberBasicView, String>();
        mail = new TableColumn<MemberBasicView, String>();
        surname = new TableColumn<MemberBasicView, String>();
        name = new TableColumn<MemberBasicView, String>();
    }

    @FXML
    private void initialize() {
        memberRepository = new MemberRepository();
        memberService = new MemberService(memberRepository);

        member_id.setCellValueFactory(new PropertyValueFactory<MemberBasicView, Long>("member_id"));
        city.setCellValueFactory(new PropertyValueFactory<MemberBasicView, String>("city"));
        mail.setCellValueFactory(new PropertyValueFactory<MemberBasicView, String>("mail"));
        surname.setCellValueFactory(new PropertyValueFactory<MemberBasicView, String>("surname"));
        name.setCellValueFactory(new PropertyValueFactory<MemberBasicView, String>("name"));



        ObservableList<MemberBasicView> observablePersonsList = initializePersonsData();
        systemPersonsTableView.setItems(observablePersonsList);

        systemPersonsTableView.getSortOrder().add(member_id);

        initializeTableViewSelection();
        loadIcons();

        logger.info("PersonsController initialized");
    }

    private void initializeTableViewSelection() {
        MenuItem edit = new MenuItem("Edit member");
        MenuItem detailedView = new MenuItem("Detailed member view");
        MenuItem delete = new MenuItem("Delete member");
        edit.setOnAction((ActionEvent event) -> {
            MemberBasicView personView = systemPersonsTableView.getSelectionModel().getSelectedItem();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(App.class.getResource("fxml/MemberEdit.fxml"));
                Stage stage = new Stage();
                stage.setUserData(personView);
                stage.setTitle("BDS JavaFX Edit Person");

                MemberEditController controller = new MemberEditController();
                controller.setStage(stage);
                fxmlLoader.setController(controller);

                Scene scene = new Scene(fxmlLoader.load(), 600, 500);

                stage.setScene(scene);

                stage.show();
            } catch (IOException ex) {
                ExceptionHandler.handleException(ex);
            }
        });

        detailedView.setOnAction((ActionEvent event) -> {
            MemberBasicView memberView = systemPersonsTableView.getSelectionModel().getSelectedItem();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(App.class.getResource("fxml/MemberDetailView.fxml"));
                Stage stage = new Stage();

                Long personId = memberView.getMember_Id();
                MemberDetailView memberDetailView = memberService.getMemberDetailView(personId);

                stage.setUserData(memberDetailView);
                stage.setTitle("BDS JavaFX Persons Detailed View");

                MemberDetailViewController controller = new MemberDetailViewController();
                controller.setStage(stage);
                fxmlLoader.setController(controller);

                Scene scene = new Scene(fxmlLoader.load(), 600, 500);

                stage.setScene(scene);

                stage.show();
            } catch (IOException ex) {
                ExceptionHandler.handleException(ex);
            }
        });

        delete.setOnAction((ActionEvent event) -> {
            MemberBasicView memberBasicView = systemPersonsTableView.getSelectionModel().getSelectedItem();
            Long member_id = memberBasicView.getMember_Id();
            String name  = memberBasicView.getSurname();
            memberRepository.removeMember(member_id);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Member deleted");
            alert.setHeaderText("Member deleted: " + name);
            alert.showAndWait();
            handleRefreshButton(null);
        });


        ContextMenu menu = new ContextMenu();
        menu.getItems().add(edit);
        menu.getItems().addAll(detailedView);
        menu.getItems().add(delete);
        systemPersonsTableView.setContextMenu(menu);
    }

    private ObservableList<MemberBasicView> initializePersonsData() {
        List<MemberBasicView> persons = memberService.getMembersBasicView();
        return FXCollections.observableArrayList(persons);
    }

    private void loadIcons() {
        Image vutLogoImage = new Image(App.class.getResourceAsStream("logos/vut-logo-eng.png"));
        ImageView vutLogo = new ImageView(vutLogoImage);
        vutLogo.setFitWidth(150);
        vutLogo.setFitHeight(50);
    }

    public void handleExitMenuItem(ActionEvent event) {
        System.exit(0);
    }

    public void handleAddPersonButton(ActionEvent actionEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/MemberCreate.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 600, 500);
            Stage stage = new Stage();
            stage.setTitle("BDS JavaFX Create Person");
            stage.setScene(scene);

            stage.show();
        } catch (IOException ex) {
            ExceptionHandler.handleException(ex);
        }
    }

    public void handleRefreshButton(ActionEvent actionEvent) {
        ObservableList<MemberBasicView> observablePersonsList = initializePersonsData();
        systemPersonsTableView.setItems(observablePersonsList);
        systemPersonsTableView.refresh();
        systemPersonsTableView.sort();
    }

    public void handleFilterButton(ActionEvent actionEvent){
        try {
            String text = searchBar.getText();
            System.out.println("handler" +text);
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/MemberFilter.fxml"));
            Stage stage = new Stage();
            MemberFilterController memberFilterController = new MemberFilterController();
            stage.setUserData(text);
            memberFilterController.setStage(stage);
            fxmlLoader.setController(memberFilterController);
            Scene scene = new Scene(fxmlLoader.load(), 600, 500);

            stage.setTitle("Member filtered view");
            stage.setScene(scene);
            stage.show();

        } catch (IOException ex) {
            ExceptionHandler.handleException(ex);
        }

    }

    public void handleInjectionButton (ActionEvent actionEvent){
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("fxml/MemberInjection.fxml"));
            Stage stage = new Stage();
            MemberInjectionController MemberinjectionController = new MemberInjectionController();

            MemberinjectionController.setStage(stage);
            fxmlLoader.setController(MemberinjectionController);
            Scene scene = new Scene(fxmlLoader.load(), 600, 500);

            stage.setTitle("SQL Injection table");
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            ExceptionHandler.handleException(ex);
        }
    }


}
