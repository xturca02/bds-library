package org.vut.feec.library.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.vut.feec.library.api.MemberInjectionView;
import org.vut.feec.library.data.MemberRepository;
import org.vut.feec.library.services.MemberService;

import java.util.List;

public class MemberInjectionController {
    @FXML
    private TableColumn<MemberInjectionView, Long> member_id;
    @FXML
    private TableColumn<MemberInjectionView, String> name;
    @FXML
    private TableColumn<MemberInjectionView, String> surname;

    @FXML
    private TableView<MemberInjectionView> systemMembersTableView;

    @FXML
    private TextField inputField;

    private MemberService memberService;
    private MemberRepository memberRepository;

    public Stage stage;
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void initialize() {
        memberRepository = new MemberRepository();
        memberService = new MemberService(memberRepository);

        member_id.setCellValueFactory(new PropertyValueFactory<MemberInjectionView, Long>("member_id"));
        name.setCellValueFactory(new PropertyValueFactory<MemberInjectionView, String>("name"));
        surname.setCellValueFactory(new PropertyValueFactory<MemberInjectionView, String>("surname"));

    }
    private ObservableList<MemberInjectionView> initializeMemberData() {

        String input = inputField.getText();
        List<MemberInjectionView> member = memberService.getMemberInjectionView(input);
        return FXCollections.observableArrayList(member);
    }

    public void handleConfirmButton(ActionEvent actionEvent){
        ObservableList<MemberInjectionView> observableMemberList = initializeMemberData();
        systemMembersTableView.setItems(observableMemberList);
    }

}
