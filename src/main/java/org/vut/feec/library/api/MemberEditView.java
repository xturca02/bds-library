package org.vut.feec.library.api;

public class MemberEditView {

    private Long id;
    private String mail;
    private String name;
    private String membership;
    private String surname;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setName(String mail) {
        this.mail = mail;
    }

    public String getName() {
        return name;
    }

    public void setSurname(String name) {
        this.name = name;
    }


    public String getSurname() {
        return surname;
    }

    public void setMail(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "MemberEditView{" +
                "mail='" + mail + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }

}
