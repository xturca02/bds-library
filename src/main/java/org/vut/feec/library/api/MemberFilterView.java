package org.vut.feec.library.api;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MemberFilterView {
    private final LongProperty member_id = new SimpleLongProperty();
    private final StringProperty name = new SimpleStringProperty();
    private final StringProperty surname = new SimpleStringProperty();
    private final StringProperty mail = new SimpleStringProperty();

    public void setMember_Id(Long id) {
        this.member_idProperty().setValue(id);
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public void setSurname(String surname) {
        this.surname.set(surname);
    }

    public void setMail(String mail) {
        this.mail.set(mail);
    }


    public long getMember_id() {
        return member_idProperty().get();
    }

    public String getName() {
        return nameProperty().get();
    }

    public String getMail() {
        return mailProperty().get();
    }

    public String getSurname() {
        return surnameProperty().get();
    }


    public StringProperty surnameProperty() {
        return surname;
    }
    public StringProperty nameProperty() { return name; }
    public StringProperty mailProperty() {
        return mail;
    }
    public LongProperty member_idProperty() { return member_id; }

}