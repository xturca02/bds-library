package org.vut.feec.library.api;

import java.util.Arrays;

public class MemberCreateView {

    private String mail;
    private String name;
    private String surname;
    private String membership;
    private char[] pwd;

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public char[] getPwd() {return pwd;}

    public void setPwd(char[] pwd) {this.pwd = pwd;}

    @Override
    public String toString() {
        return "MemberCreateView{" +
                "mail='" + mail + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                //", pwd=" + Arrays.toString(pwd) +
                '}';
    }
}
