package org.vut.feec.library.api;

import javafx.beans.property.*;

public class MemberBasicView {
    private final LongProperty member_id = new SimpleLongProperty();
    private final StringProperty city = new SimpleStringProperty();
    private final StringProperty mail = new SimpleStringProperty();
    private final StringProperty name = new SimpleStringProperty();
    private final StringProperty surname = new SimpleStringProperty();

    public Long getMember_Id() {
        return member_idProperty().get();
    }

    public void setMember_Id(Long id) {
        this.member_idProperty().setValue(id);
    }

    public String getCity() {
        return cityProperty().get();
    }

    public void setCity(String city) {
        this.cityProperty().setValue(city);
    }

    public String getMail() {
        return mailProperty().get();
    }

    public void setMail(String mail) {
        this.mailProperty().setValue(mail);
    }

    public String getName() {
        return nameProperty().get();
    }

    public void setName(String name) {
        this.nameProperty().setValue(name);
    }

    public String getSurname() {
        return surnameProperty().get();
    }

    public void setSurname(String surname) {
        this.surnameProperty().setValue(surname);
    }


    public LongProperty member_idProperty() {
        return member_id;
    }

    public StringProperty cityProperty() {
        return city;
    }

    public StringProperty mailProperty() {
        return mail;
    }

    public StringProperty nameProperty() {
        return name;
    }

    public StringProperty surnameProperty() {
        return surname;
    }

}