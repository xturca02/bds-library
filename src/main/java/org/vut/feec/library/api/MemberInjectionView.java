package org.vut.feec.library.api;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MemberInjectionView {
    private final LongProperty member_id = new SimpleLongProperty();
    private final StringProperty name = new SimpleStringProperty();
    private final StringProperty surname = new SimpleStringProperty();

    public void setMember_id(long member_id) {this.member_id.set(member_id);}
    public void setName(String name) {this.name.set(name);}
    public void setSurname(String surname) {this.surname.set(surname);}

    public long getMember_id() {return member_idProperty().get();}
    public String getName() {
        return nameProperty().get();
    }
    public String getSurname() {
        return surnameProperty().get();
    }

    public LongProperty member_idProperty() {
        return member_id;
    }
    public StringProperty surnameProperty() {
        return surname;
    }
    public StringProperty nameProperty() {
        return name;
    }
}