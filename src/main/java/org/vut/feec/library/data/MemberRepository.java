package org.vut.feec.library.data;

import org.vut.feec.library.api.*;
import org.vut.feec.library.config.DataSourceConfig;
import org.vut.feec.library.exceptions.DataAccessException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class MemberRepository {

    public MemberAuthView findMemberByEmail(String mail) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT mail, pwd" +
                             " FROM member m" +
                             " WHERE m.mail = ?")) {
            preparedStatement.setString(1, mail);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToMemberAuth(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find member by ID with addresses failed.", e);
        }
        return null;
    }

    public MemberDetailView findMemberDetailedView(Long member_id) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT member_id, mail, name, surname, city, house_number, street" +
                             " FROM member m" +
                             " LEFT JOIN address a ON m.member_id = a.address_id" +
                             " WHERE m.member_id = ?")) {
            preparedStatement.setLong(1, member_id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToMemberDetailView(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find member by ID with addresses failed.", e);
        }
        return null;
    }

    public List<MemberBasicView> getMembersBasicView() {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT member_id, mail, name, surname, city" +
                             " FROM member m" +
                             " LEFT JOIN address a ON m.member_id = a.address_id order by member_id");
             ResultSet resultSet = preparedStatement.executeQuery();) {
            List<MemberBasicView> memberBasicViews = new ArrayList<>();
            while (resultSet.next()) {
                memberBasicViews.add(mapToMemberBasicView(resultSet));
            }
            return memberBasicViews;
        } catch (SQLException e) {
            throw new DataAccessException("Persons basic view could not be loaded.", e);
        }
    }

    public List<MemberFilterView> getMemberFilterView(String text){
        System.out.println(text);
        String filter = '%'+text+'%';
        try (Connection connection = DataSourceConfig.getConnection();

             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT member_id, name, surname, mail" +
                             " FROM member "  +
                             " where lower(surname) like lower(?) order by member_id"
             )

        ) {
            preparedStatement.setString(1,filter);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<MemberFilterView> memberFilterViews = new ArrayList<>();
            while (resultSet.next()) {
                memberFilterViews.add(mapToMemberFilterView(resultSet));
            }
            return memberFilterViews;
        }
        catch (SQLException e) {
            throw new DataAccessException("Member basic view could not be loaded.", e);
        }
    }

    public List<MemberInjectionView> getMemberInjectionView(String input) {
        String query = "SELECT member_id,name,surname from member m where m.member_id =" + input;
        try (Connection connection = DataSourceConfig.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query)) {
            List<MemberInjectionView> injectionViews = new ArrayList<>();
            while (resultSet.next()) {
                injectionViews.add(mapToMemberInjectionView(resultSet));
            }
            return injectionViews;
        } catch (SQLException e) {
            throw new DataAccessException("Fail", e);
        }
    }


    public void createMember(MemberCreateView memberCreateView) {
        String insertPersonSQL = "INSERT INTO member (mail, name, surname, pwd ) VALUES (?,?,?,?)";
        try (Connection connection = DataSourceConfig.getConnection();
             // would be beneficial if I will return the created entity back
             PreparedStatement preparedStatement = connection.prepareStatement(insertPersonSQL, Statement.RETURN_GENERATED_KEYS)) {
            //set prepared statement variables
            preparedStatement.setString(1, memberCreateView.getMail());
            preparedStatement.setString(2, memberCreateView.getName());
            preparedStatement.setString(3, memberCreateView.getSurname());
            preparedStatement.setString(4, String.valueOf(memberCreateView.getPwd()));


            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new DataAccessException("Creating person failed, no rows affected.");
            }
        } catch (SQLException e) {
            throw new DataAccessException("Creating person failed operation on the database failed.");
        }
    }

    public void removeMember(Long member_id) {
        String deleteMemberSQL = "DELETE FROM member WHERE member_id = ?";
        System.out.println(deleteMemberSQL);
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement prpstmt = connection.prepareStatement(deleteMemberSQL)) {

            prpstmt.setLong(1, member_id);
            prpstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println("failed");
        }
    }

    public void editMember(MemberEditView memberEditView) {
        String insertPersonSQL = "UPDATE member m SET mail = ?, name = ?, surname = ? WHERE m.member_id = ?";
        String checkIfExists = "SELECT mail FROM member m WHERE m.member_id = ?";
        try (Connection connection = DataSourceConfig.getConnection();
             // would be beneficial if I will return the created entity back
             PreparedStatement preparedStatement = connection.prepareStatement(insertPersonSQL, Statement.RETURN_GENERATED_KEYS)) {
            // set prepared statement variables
            preparedStatement.setString(1, memberEditView.getMail());
            preparedStatement.setString(2, memberEditView.getName());
            preparedStatement.setString(3, memberEditView.getSurname());
            preparedStatement.setLong(4, memberEditView.getId());

            try {
                connection.setAutoCommit(false);
                try (PreparedStatement ps = connection.prepareStatement(checkIfExists, Statement.RETURN_GENERATED_KEYS)) {
                    ps.setLong(1, memberEditView.getId());
                    ps.execute();
                } catch (SQLException e) {
                    throw new DataAccessException("This person for edit do not exists.");
                }

                int affectedRows = preparedStatement.executeUpdate();

                if (affectedRows == 0) {
                    throw new DataAccessException("Creating person failed, no rows affected.");
                }
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
            } finally {
                connection.setAutoCommit(true);
            }
        } catch (SQLException e) {
            throw new DataAccessException("Creating person failed operation on the database failed.");
        }
    }


    private MemberAuthView mapToMemberAuth(ResultSet rs) throws SQLException {
        MemberAuthView member = new MemberAuthView();
        member.setMail(rs.getString("mail"));
        member.setPassword(rs.getString("pwd"));
        return member;
    }

    private MemberBasicView mapToMemberBasicView(ResultSet rs) throws SQLException {
        MemberBasicView memberBasicView = new MemberBasicView();
        memberBasicView.setMember_Id(rs.getLong("member_id"));
        memberBasicView.setMail(rs.getString("mail"));
        memberBasicView.setName(rs.getString("name"));
        memberBasicView.setSurname(rs.getString("surname"));
        memberBasicView.setCity(rs.getString("city"));
        return memberBasicView;
    }

    private MemberDetailView mapToMemberDetailView(ResultSet rs) throws SQLException {
        MemberDetailView memberDetailView = new MemberDetailView();
        memberDetailView.setMember_Id(rs.getLong("member_id"));
        memberDetailView.setMail(rs.getString("mail"));
        memberDetailView.setName(rs.getString("name"));
        memberDetailView.setSurname(rs.getString("surname"));
        memberDetailView.setCity(rs.getString("city"));
        memberDetailView.sethouseNumber(rs.getString("house_number"));
        memberDetailView.setStreet(rs.getString("street"));
        return memberDetailView;
    }

    private MemberInjectionView mapToMemberInjectionView(ResultSet rs ) throws  SQLException{
        MemberInjectionView injectionView = new MemberInjectionView();
        injectionView.setMember_id(rs.getLong("member_id"));
        injectionView.setName(rs.getString("name"));
        injectionView.setSurname(rs.getString("surname"));
        return injectionView;
    }

    private MemberFilterView mapToMemberFilterView(ResultSet rs) throws SQLException{
        MemberFilterView memberFilterView = new MemberFilterView();
        memberFilterView.setMember_Id(rs.getLong("member_id"));
        memberFilterView.setName(rs.getString("name"));
        memberFilterView.setSurname(rs.getString("surname"));
        memberFilterView.setMail(rs.getString("mail"));
        return memberFilterView;
    }
}
