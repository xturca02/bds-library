package org.vut.feec.library.services;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.vut.feec.library.api.*;
import org.vut.feec.library.data.MemberRepository;
import java.util.List;


public class MemberService {

    private MemberRepository memberRepository;

    public MemberService(MemberRepository memberRepository) {
        this.memberRepository = memberRepository;
    }

    public MemberDetailView getMemberDetailView(Long id) {
        return memberRepository.findMemberDetailedView(id);
    }

    public List<MemberBasicView> getMembersBasicView() {
        return memberRepository.getMembersBasicView();
    }


    public List<MemberInjectionView> getMemberInjectionView(String input){return memberRepository.getMemberInjectionView(input);
    }

    public List<MemberFilterView> getMemberFilterView(String text){
        return memberRepository.getMemberFilterView(text);
    }

    public void createMember(MemberCreateView memberCreateView) {
        memberRepository.createMember(memberCreateView);
    }

    public void editMember(MemberEditView memberEditView) {
        memberRepository.editMember(memberEditView);
    }

    /**
     * <p>
     * Note: For implementation details see: https://github.com/patrickfav/bcrypt
     * </p>
     *
     * @param password to be hashed
     * @return hashed password
     */
    private char[] hashPassword(char[] password) {
        return BCrypt.withDefaults().hashToChar(12, password);
    }

}
