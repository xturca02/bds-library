# Project 3 - Database Application
### Library Database
The final project for the BPC-BDS course at BUT.
## To build and run the project
To build and run the project, enter this to the project root directory:

```ruby
mvn clean install
```

```ruby
java -jar bds-library-1.0.0.jar
```

## Licenses
licenses &#8594; dependencies.html

## Logs
src/main &#8594; resources &#8594; logback.xml

------------------
------------------
Klára Turčanová  [ 227248 ]
